/**
 * @file
 */

(function ($, Drupal) {

  $.fn.sendToContent = function() {
    $('.send-to-content a.visually-hidden').on('focusin', function() {
      $(this).removeClass('visually-hidden');
      $(this).addClass('visually-show');
    });

    $('.send-to-content a.visually-show').on('focusout', function() {
      $(this).removeClass('visually-show');
      $(this).addClass('visually-hidden');
    });
  };

  Drupal.behaviors.arborist = {
    attach: function (context, settings) {

      $(document).ready(function(){
        $().sendToContent();
      });

    }
  };

})(jQuery, Drupal);